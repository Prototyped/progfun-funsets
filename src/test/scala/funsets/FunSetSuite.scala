package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
      assert(!exists(s, (x: Int) => !(contains(s1, x) || contains(s2, x))),
              "s1 U s2 contains no other bounded int")
     }
   }

  test("contains returns true for elements within the set") {
    new TestSets {
      assert(contains(s1, 1), "s1 contains 1")
      assert(!contains(s2, 1), "s2 does not contain 1")
    }
  }

  test("singetonSet contains only the element from which it was constructed") {
    new TestSets {
      assert(contains(s1, 1), "s1 contains 1")
      assert(!exists(s1, _ != 1),
             "s1 does not contain any other bounded integer")
    }
  }

  test("intersect contains only common elements") {
    new TestSets {
      val u1 = union(s1, s2)
      val u2 = union(s2, s3)
      val i = intersect(u1, u2)
      assert(contains(i, 2), "intersection contains the common element 2")
      assert(!exists(i, !contains(s2, _)),
             "i does not contain any other bounded integer")
    }
  }

  test("intersect of a set with an empty set is an empty set") {
    new TestSets {
      val i = intersect(s1, _ => false)
      assert(!exists(i, _ => true), "Intersection is empty")
    }
  }

  test("union of a set with an empty set is the original set") {
    new TestSets {
      val u = union(s1, _ => false)
      assert(!exists(u, !contains(s1, _)), "Union is identical to s1")
    }
  }

  test("diff contains only elements from first set that are not in second") {
    new TestSets {
      val u1 = union(s1, union(s2, s3))
      val u2 = union(s1, s2)
      val d = diff(u1, u2)
      assert(contains(d, 3), "difference contains the element 3")
      assert(!exists(d, !contains(s3, _)),
             "d does not contain any other bounded integer")
    }
  }

  test("diff between an empty set and any set is the empty set") {
    new TestSets {
      val d = diff(_ => false, s1)
      assert(!exists(d, _ => true), "d is empty")
    }
  }

  test("diff between any set and the empty set is the original set") {
    new TestSets {
      val d = diff(s1, _ => false)
      assert(!exists(d, !contains(s1, _)),
             "d contains no other bounded integer")
    }
  }

  test("forall correctness") {
    new TestSets {
      assert(forall((x: Int) => false, (x: Int) => true),
             "forall over the empty set returns true")
      assert(forall(s1, s1),
             "forall over singleton with itself as predicate returns true")
      assert(!forall(s1, s2),
             "forall over singleton with other singleton as predicate is false")
      def evenNumbers(x: Int): Boolean = (x & 1) == 0
      assert(forall(evenNumbers, _ => true),
             "forall with predicate that is always true is true")
      assert(!forall(evenNumbers, _ != 0),
             "forall with predicate that is only false once is false")
      assert(forall(evenNumbers, _ != 1),
             "forall with predicate that is false only for integers not in set is true")
      assert(forall(evenNumbers, _ != 2000),
             "forall with predicate that is false only for integers greater than upper bound is true")
      assert(forall(evenNumbers, _ != -2000),
             "forall with predicate that is false only for integers less than lower bound is true")
      assert(!forall(evenNumbers, _ != 1000),
             "forall with predicate that is false only for upper bound is false")
      assert(!forall(evenNumbers, _ != -1000),
             "forall with predicate that is false only for lower bound is false")
    }
  }

  test("exists correctness") {
    new TestSets {
      assert(!exists((x: Int) => false, (x: Int) => true),
             "exists over the empty set returns false (not even one element passes the predicate)")
      assert(exists(s1, s1),
             "exists over singleton with itself as predicate returns true")
      assert(!exists(s1, s2),
             "exists over singleton with other singleton as predicate is false")
      def evenNumbers(x: Int): Boolean = (x & 1) == 0
      assert(exists(evenNumbers, _ => true),
             "exists with predicate that is always true is true")
      assert(exists(evenNumbers, _ != 0),
             "exists with predicate that is only false once is true")
      assert(exists(evenNumbers, _ == 0),
             "exists with predicate that is only true once is true")
      assert(exists(evenNumbers, _ != 1),
             "exists with predicate that is false only for integers not in set is true")
      assert(!exists(evenNumbers, _ == 1),
             "exists with predicate that is false for all set elements is false")
      assert(exists(evenNumbers, _ != 2000),
             "exists with predicate that is false only for integers greater than upper bound is true")
      assert(exists(evenNumbers, _ != -2000),
             "exists with predicate that is false only for integers less than lower bound is true")
      assert(exists(evenNumbers, _ != 1000),
             "exists with predicate that is false only for upper bound is true")
      assert(exists(evenNumbers, _ != -1000),
             "exists with predicate that is false only for lower bound is true")
    }
  }

  test("map correctness") {
    new TestSets {
      assert(forall(map(_ => true, _ => 1), _ == 1),
             "map with constant transform function produces a singleton")
      assert(!exists(map(_ => false, _ => 1), _ => true),
             "map over empty set produces an empty set")
      def evenNumbers(x: Int): Boolean = (x & 1) == 0
      def identity(x: Int): Int = x
      assert(forall(map(evenNumbers, identity), evenNumbers),
             "map with identity function returns set with all elements of pre-map set")
      assert(!exists(map(evenNumbers, identity), !evenNumbers(_)),
             "map with identity function returns set with no additional elements")
      def isMultipleOfFour(x: Int): Boolean = (x & 3) == 0
      assert(forall(map(evenNumbers, _ * 2), isMultipleOfFour),
             "map with transformative function returns transformed set")
      assert(!exists(map(evenNumbers, _ * 2), !isMultipleOfFour(_)),
             "map with transformative function returns set with no elements not produced by transformation")
    }
  }

  test("filter correctness") {
    new TestSets {
      assert(!exists(filter(_ => false, _ => true), _ => true),
             "filter over empty set produces the empty set")
      assert(!exists(filter(_ => true, _ => false), _ => true),
             "filter over a set with a predicate that is never true produces the empty set")
      def evenNumbers(x: Int): Boolean = (x & 1) == 0
      assert(forall(filter(evenNumbers, _ => true), evenNumbers),
             "filter over a set with a predicate that is always true produces a set containing the original elements")
      assert(!exists(filter(evenNumbers, _ => true), !evenNumbers(_)),
             ". . . and no other elements")
      assert(forall(filter(evenNumbers, _ != 1), evenNumbers),
             "filter over a set with a predicate that is true only for non-set elements has all elements from the original set")
      assert(!exists(filter(evenNumbers, _ != 1), !evenNumbers(_)),
             ". . . and no other elements")
      def multiplesOfFour(x: Int): Boolean = (x & 3) == 0
      assert(forall(filter(evenNumbers, multiplesOfFour), multiplesOfFour),
             "filter over a set with a more restrictive predicate has elements for which the predicate is true that are also in the original set")
      assert(!exists(filter(evenNumbers, multiplesOfFour), !multiplesOfFour(_)),
             ". . . and no others")
    }
  }
}
